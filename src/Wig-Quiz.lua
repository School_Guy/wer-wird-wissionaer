require "global"
require "Messages"
require "Fragen"

cr = [[

]]
UsedJokers = { "50:50 Joker", "+30 sec Joker", "Frage tauschen Joker" }
pStart = 0
ClickedAnswer = 0
Points = 0
CorrectAnswers = 0
FalseAnswers = 0
TotalAnswers = 0
TooLate = 0
CdTime = 30
ActiveJokerNumber = 0
rnd1 = 0
DoubleTimeJoker = false
FitftyFitftyJoker = false
ZoneHaveToMove = false
QuestionActive = false
GPSTestActive = false
PlayerPlatform = "Android"
language = LANGUAGE_DE

function OnStartCatridge()
    GPSTestActive = false
    local text = getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 1) .. Player.Name
    text = text .. getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 2) .. cr
    text = text .. getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 3) .. cr
    text = text .. getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 4)
    msgMitNachrichtUndBild(text,
        P_talkmaster,
        function()
            text = getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 5) .. cr
            text = text .. getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 6)
            msgMitNachrichtUndBild(text,
                P_talkmaster,
                function()
                    text = getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 7) .. Player.Name
                    text = text .. getMessage(language, SECTION_ITEM_START_GAME, SUBSECTION_STARTGAME_CMD02, 8)
                    msgMitNachricht(text,
                        function()
                            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                        end)
                end)
        end)
    O_Question:MoveTo(Player)
    O_resetZone:MoveTo(Player)
    T_Progress.Active = true
    T_playingRules.Active = true
    T_50_50_Joker.Active = true
    T_DoubleTime_Joker.Active = true
    T_Change_Question_Joker.Active = true
    initializeVariables()
    setZone(Z_Question, Player.ObjectLocation, 0)
    Aktualisierung:Start()
end

function OnResume()
    Wherigo.Command "StopSound"
    pStart = Player.ObjectLocation
    if ZoneHaveToMove == true then
        Aktualisierung:Start()
    elseif QuestionActive == true then
        Cd_Question_Joker.Duration = 1
        Cd_Question_Joker.Remaining = Cd_Question_Joker.Duration
        Cd_Question_Joker.Elapsed = 0
        Cd_TimeToAnswer.Duration = 1
        Cd_TimeToAnswer.Remaining = Cd_TimeToAnswer.Duration
        Cd_TimeToAnswer.Elapsed = 0
        Cd_SecondTimer.Duration = 1
        Cd_SecondTimer.Remaining = 1
        Cd_SecondTimer.Elapsed = 0
        if Cd_Question_Joker.Remaining == 0 then
            Cd_TimeToAnswer:Start()
        else
            Cd_Question_Joker:Start()
        end
        Cd_SecondTimer:Start()
    elseif GPSTestActive == true then
        Cd_TestGPS:Start()
    end
end

function OnEnterZ_Question()
    Aktualisierung:Stop()
    Z_Question.Active = false
    ZoneHaveToMove = false
    O_Question.Commands.Cmd05.Enabled = true
    if DoubleTimeJoker == false then
        Cd_Question_Joker.Duration = 20
        Cd_Question_Joker.Remaining = Cd_Question_Joker.Duration
        Cd_Question_Joker.Elapsed = 0
        Cd_TimeToAnswer.Duration = 10
        Cd_TimeToAnswer.Remaining = Cd_TimeToAnswer.Duration
        Cd_TimeToAnswer.Elapsed = 0
        CdTime = 30
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            Wherigo.PlayAudio(S_30sec)
        end
    end
    variableZuruecksetzen()
    getTable()
    local text = ActiveQuestion .. cr .. "A = " .. ActiveAnswer1 .. cr .. "B = " .. ActiveAnswer2 .. cr .. "C = "
    text = text .. ActiveAnswer3
    O_Question.Description = text
    Cd_Question_Joker:Start()
    Cd_SecondTimer:Start()
    O_Question.Media = P_blank
    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, O_Question)
end

function OnClickO_FinishCmd01()
    local sec = SECTION_ITEM_FINSIH
    local subsec = SUBSECTION_FINISH_CMD01
    local text = getMessage(language, sec, subsec, 1) .. cr .. getMessage(language, sec, subsec, 2) .. cr
    text = text .. getMessage(language, sec, subsec, 3) .. cr .. getMessage(language, sec, subsec, 4) .. cr
    text = text .. getMessage(language, sec, subsec, 5) .. cr .. getMessage(language, sec, subsec, 6) .. cr
    text = text .. getMessage(language, sec, subsec, 7) .. cr .. getMessage(language, sec, subsec, 8) .. cr
    text = text .. getMessage(language, sec, subsec, 9) .. cr .. getMessage(language, sec, subsec, 10) .. cr
    text = text .. getMessage(language, sec, subsec, 11) .. cr .. getMessage(language, sec, subsec, 12) .. cr
    text = text .. getMessage(language, sec, subsec, 13)
    msgMitNachricht(text,
        function()
            text = getMessage(language, sec, subsec, 14) .. cr .. getMessage(language, sec, subsec, 15)
            msgMitNachricht(text,
                function()
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end)
        end)
end

function OnClickO_FinishCmd02()
    local sec = SECTION_ITEM_FINSIH
    local subsec = SUBSECTION_FINISH_CMD02
    local text = getMessage(language, sec, subsec, 1) .. string.sub(Player.CompletionCode, 1, 15) .. cr
    text = text .. getMessage(language, sec, subsec, 2)
    msgMitNachricht(text,
        function()
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function OnClickO_QuestionCmd01()
    O_Question.Commands.Cmd01.Enabled = false
    O_Question.Commands.Cmd02.Enabled = false
    O_Question.Commands.Cmd03.Enabled = false
    Cd_TimeToAnswer:Stop()
    QuestionHandling(2)
end

function OnClickO_QuestionCmd02()
    O_Question.Commands.Cmd01.Enabled = false
    O_Question.Commands.Cmd02.Enabled = false
    O_Question.Commands.Cmd03.Enabled = false
    Cd_TimeToAnswer:Stop()
    QuestionHandling(3)
end

function OnClickO_QuestionCmd03()
    O_Question.Commands.Cmd01.Enabled = false
    O_Question.Commands.Cmd02.Enabled = false
    O_Question.Commands.Cmd03.Enabled = false
    Cd_TimeToAnswer:Stop()
    QuestionHandling(4)
end

function OnClickO_QuestionCmd05()
    local NumberOfJokers = TableLength(UsedJokers)
    if NumberOfJokers > 0 then
        I_Joker.Choices = UsedJokers
    else
        UsedJokers = { "Keine Joker uebrig" }
        I_Joker.Choices = UsedJokers
        Wherigo.ShowScreen(Wherigo.DETAILSCREEN, O_Question)
    end
    Wherigo.GetInput(I_Joker)
end

function OnGetInputI_Joker(input)
    if input == nil then
        input = ""
    end

    local sec = SECTION_INPUT_JOKER
    local subsec = SUBSECTION_INPUT_ON_GET_INPUT
    O_Question.Commands.Cmd01.Enabled = false
    O_Question.Commands.Cmd02.Enabled = false
    O_Question.Commands.Cmd03.Enabled = false
    if Wherigo.NoCaseEquals(input, "50:50 Joker") then
        for i = 0, 3, 1 do
            if UsedJokers[i] == "50:50 Joker" then
                ActiveJokerNumber = i
            end
        end
        table.remove(UsedJokers, ActiveJokerNumber)
        rnd1 = math.random(1, 3)
        while rnd1 + 1 == ActiveCorrectAnswer do
            rnd1 = math.random(1, 3)
        end
        if rnd1 == 1 then
            O_Question.Description = O_Question.Description .. cr .. getMessage(language, sec, subsec, 1)
        elseif rnd1 == 2 then
            O_Question.Description = O_Question.Description .. cr .. getMessage(language, sec, subsec, 2)
        elseif rnd1 == 3 then
            O_Question.Description = O_Question.Description .. cr .. getMessage(language, sec, subsec, 3)
        end
        if CdTime <= 10 then
            if rnd1 == 1 then
                O_Question.Commands.Cmd05.Enabled = false
                O_Question.Commands.Cmd01.Enabled = false
                O_Question.Commands.Cmd02.Enabled = true
                O_Question.Commands.Cmd03.Enabled = true
            elseif rnd1 == 2 then
                O_Question.Commands.Cmd05.Enabled = false
                O_Question.Commands.Cmd01.Enabled = true
                O_Question.Commands.Cmd02.Enabled = false
                O_Question.Commands.Cmd03.Enabled = true
            elseif rnd1 == 3 then
                O_Question.Commands.Cmd05.Enabled = false
                O_Question.Commands.Cmd01.Enabled = true
                O_Question.Commands.Cmd02.Enabled = true
                O_Question.Commands.Cmd03.Enabled = false
            end
        end
        FitftyFitftyJoker = true
        T_50_50_Joker.Active = false
    elseif Wherigo.NoCaseEquals(input, "+30 sec Joker") then
        for i = 0, 3, 1 do
            if UsedJokers[i] == "+30 sec Joker" then
                ActiveJokerNumber = i
            end
        end
        table.remove(UsedJokers, ActiveJokerNumber)
        Cd_Question_Joker:Stop()
        Cd_TimeToAnswer:Stop()
        Cd_SecondTimer:Stop()
        Cd_Question_Joker.Duration = 20 + CdTime
        Cd_Question_Joker.Remaining = Cd_Question_Joker.Duration
        Cd_Question_Joker.Elapsed = 0
        Cd_TimeToAnswer.Duration = 10
        Cd_TimeToAnswer.Remaining = 10
        Cd_TimeToAnswer.Elapsed = 0
        O_Question.Commands.Cmd05.Enabled = true
        CdTime = CdTime + 30
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            Wherigo.Command "StopSound"
            ChooseSound()
        end
        DoubleTimeJoker = true
        T_DoubleTime_Joker.Active = false
        Cd_Question_Joker:Start()
        Cd_SecondTimer:Start()
    elseif Wherigo.NoCaseEquals(input, "Frage tauschen Joker") then
        for i = 0, 3, 1 do
            if UsedJokers[i] == "Frage tauschen Joker" then
                ActiveJokerNumber = i
            end
        end
        Cd_Question_Joker:Stop()
        Cd_TimeToAnswer:Stop()
        table.remove(UsedJokers, ActiveJokerNumber)
        OnEnterZ_Question()
        T_Change_Question_Joker.Active = false
    elseif Wherigo.NoCaseEquals(input, "Keine Joker uebrig") then
        Wherigo.ShowScreen(Wherigo.DETAILSCREEN, O_Question)
    end
    if CdTime < 10 then
        O_Question.Commands.Cmd01.Enabled = true
        O_Question.Commands.Cmd02.Enabled = true
        O_Question.Commands.Cmd03.Enabled = true
    end
end

function OnStartAktualisierung()
    pStart = Player.ObjectLocation
end

function OnElapseAktualisierung()
    moveZone(50, Z_Question, pStart, true)
end

function OnStartCd_Question_Joker()
    --Nothing Happens
end

function OnElapseCd_Question_Joker()
    if FitftyFitftyJoker == true then
        if rnd1 == 1 then
            O_Question.Commands.Cmd05.Enabled = false
            O_Question.Commands.Cmd02.Enabled = true
            O_Question.Commands.Cmd03.Enabled = true
        elseif rnd1 == 2 then
            O_Question.Commands.Cmd05.Enabled = false
            O_Question.Commands.Cmd01.Enabled = true
            O_Question.Commands.Cmd03.Enabled = true
        elseif rnd1 == 3 then
            O_Question.Commands.Cmd05.Enabled = false
            O_Question.Commands.Cmd01.Enabled = true
            O_Question.Commands.Cmd02.Enabled = true
        end
    else
        O_Question.Commands.Cmd05.Enabled = false
        O_Question.Commands.Cmd03.Enabled = true
        O_Question.Commands.Cmd02.Enabled = true
        O_Question.Commands.Cmd01.Enabled = true
    end
    Cd_Question_Joker.Duration = 20
    Cd_TimeToAnswer:Start()
end

function OnStartCd_TimeToAnswer()
    --Nothing Happens
end

function OnElapseCd_TimeToAnswer()
    O_Question.Commands.Cmd01.Enabled = false
    O_Question.Commands.Cmd02.Enabled = false
    O_Question.Commands.Cmd03.Enabled = false
    QuestionHandling(0)
end

function OnStartCd_SecondTimer()
    --Nothing happens here
end

function OnElapseCd_SecondTimer()
    CdTime = CdTime - 1
    if CdTime > 0 then
        Cd_SecondTimer:Start()
    elseif CdTime == 0 then
        CdTime = 30
    end
    if CdTime % 5 == 0 then
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            --Do Nothing
        else
            Wherigo.PlayAudio(S_GarminBeep)
        end
    end
end

function OnStartCd_TestGPS()
    --Nothing happens
end

function OnElapseCd_TestGPS()
    local sec = SECTION_TIMER_TEST_GPS
    local subsec = SUBSECTION_TIMER_ON_ELAPSED
    if GPSOK == false then
        GPSTestActive = true
        local text = getMessage(language, sec, subsec, 1)
        for i = 0, countGPS, 1 do
            text = text .. "."
        end
        O_StartGame.Description = text
        testGPS()
    elseif GPSOK == true then
        O_StartGame.Description = getMessage(language, sec, subsec, 2)
        O_StartGame.Commands.Cmd02.Enabled = true
    else
        Wherigo.LogMessage("GPSOK was neither true or false.", Wherigo.LOGERROR)
    end
end

function ChooseSound()
    if CdTime == 60 then
        Wherigo.PlayAudio(S_60sec)
    elseif CdTime == 59 then
        Wherigo.PlayAudio(S_59sec)
    elseif CdTime == 58 then
        Wherigo.PlayAudio(S_58sec)
    elseif CdTime == 57 then
        Wherigo.PlayAudio(S_57sec)
    elseif CdTime == 56 then
        Wherigo.PlayAudio(S_56sec)
    elseif CdTime == 55 then
        Wherigo.PlayAudio(S_55sec)
    elseif CdTime == 54 then
        Wherigo.PlayAudio(S_54sec)
    elseif CdTime == 53 then
        Wherigo.PlayAudio(S_53sec)
    elseif CdTime == 52 then
        Wherigo.PlayAudio(S_52sec)
    elseif CdTime == 51 then
        Wherigo.PlayAudio(S_51sec)
    elseif CdTime == 50 then
        Wherigo.PlayAudio(S_50sec)
    elseif CdTime == 49 then
        Wherigo.PlayAudio(S_49sec)
    elseif CdTime == 48 then
        Wherigo.PlayAudio(S_48sec)
    elseif CdTime == 47 then
        Wherigo.PlayAudio(S_47sec)
    elseif CdTime == 46 then
        Wherigo.PlayAudio(S_46sec)
    elseif CdTime == 45 then
        Wherigo.PlayAudio(S_45sec)
    elseif CdTime == 44 then
        Wherigo.PlayAudio(S_44sec)
    elseif CdTime == 43 then
        Wherigo.PlayAudio(S_43sec)
    elseif CdTime == 42 then
        Wherigo.PlayAudio(S_42sec)
    elseif CdTime == 41 then
        Wherigo.PlayAudio(S_41sec)
    elseif CdTime == 40 then
        Wherigo.PlayAudio(S_40sec)
    elseif CdTime == 39 then
        Wherigo.PlayAudio(S_39sec)
    elseif CdTime == 38 then
        Wherigo.PlayAudio(S_38sec)
    elseif CdTime == 37 then
        Wherigo.PlayAudio(S_37sec)
    elseif CdTime == 36 then
        Wherigo.PlayAudio(S_36sec)
    elseif CdTime == 35 then
        Wherigo.PlayAudio(S_35sec)
    elseif CdTime == 34 then
        Wherigo.PlayAudio(S_34sec)
    elseif CdTime == 33 then
        Wherigo.PlayAudio(S_33sec)
    elseif CdTime == 32 then
        Wherigo.PlayAudio(S_32sec)
    elseif CdTime == 31 then
        Wherigo.PlayAudio(S_31sec)
    elseif CdTime == 30 then
        Wherigo.PlayAudio(S_30sec)
    end
end

function QuestionHandling(ClickedAnswer)
    local sec = SECTION_FUNCTIONS
    local subsec = SUBSECTION_QUESTIONHANDLING
    local text = ""
    QuestionActive = false
    TotalAnswers = TotalAnswers + 1
    if DoubleTimeJoker == true then
        Cd_TimeToAnswer.Duration = 10
    end
    if ClickedAnswer == ActiveCorrectAnswer then
        CorrectAnswers = CorrectAnswers + 1
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            Wherigo.Command "StopSound"
            Wherigo.PlayAudio(S_win)
        end
        PointsHandling(1)
        O_Question.Media = P_talkmaster
        O_Question.Description = getMessage(language, sec, subsec, 1)
        text = getMessage(language, sec, subsec, 2) .. cr .. cr .. getMessage(language, sec, subsec, 3)
        text = text .. Points .. getMessage(language, sec, subsec, 4)
        msgMitNachrichtUndBild(text, P_winSmiley,
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                WinMessage()
            end)
    elseif ClickedAnswer == 0 then
        TooLate = TooLate + 1
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            Wherigo.Command "StopSound"
            Wherigo.PlayAudio(S_fail)
        end
        PointsHandling(2)
        O_Question.Media = P_talkmaster
        O_Question.Description = getMessage(language, sec, subsec, 5)
        text = getMessage(language, sec, subsec, 6) .. cr .. cr .. getMessage(language, sec, subsec, 7)
        text = text .. ActiveTable[ActiveCorrectAnswer] .. cr .. cr .. getMessage(language, sec, subsec, 3) .. Points
        text = text .. getMessage(language, sec, subsec, 4)
        msgMitNachrichtUndBild(text,
            P_sadSmiley,
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        FalseAnswers = FalseAnswers + 1
        if PlayerPlatform == "Android" or PlayerPlatform == "IPhone" then
            Wherigo.Command "StopSound"
            Wherigo.PlayAudio(S_fail)
        end
        PointsHandling(0)
        O_Question.Media = P_talkmaster
        O_Question.Description = getMessage(language, sec, subsec, 8)
        local cor_answer = ActiveTable[ActiveCorrectAnswer]
        local cho_answer = ActiveTable[ClickedAnswer]
        text = getMessage(language, sec, subsec, 9) .. cr .. cr .. getMessage(language, sec, subsec, 7) .. cor_answer
        text = text .. cr .. getMessage(language, sec, subsec, 10) .. cho_answer .. cr .. cr
        text = text .. getMessage(language, sec, subsec, 3) .. Points .. getMessage(language, sec, subsec, 4)
        msgMitNachrichtUndBild(text,
            P_sadSmiley,
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
    Z_Question.Active = false
    if Points < 20 then
        variableZuruecksetzen()
        Aktualisierung:Start()
        setZone(Z_Question, Player.ObjectLocation, 0)
    end
    FitftyFitftyJoker = false
    DoubleTimeJoker = false
    ZoneHaveToMove = true
    Save()
end

function PointsHandling(correct)
    local sec = SECTION_FUNCTIONS
    local subsec = SUBSECTION_QUESTIONHANDLING
    if correct == 1 then
        Points = Points + 2
    elseif correct == 0 then
        if Points >= 2 then
            Points = Points - 2
        else
            Points = 0
        end
    else
        if Points >= 1 then
            Points = Points - 1
        else
            Points = 0
        end
    end
    ClickedAnswer = 0
    local text = getMessage(language, sec, subsec, 3) .. Points .. getMessage(language, sec, subsec, 4)
    T_Progress.Description = text
end

function WinMessage()
    if Points >= 20 then
        local sec = SECTION_FUNCTIONS
        local subsec = SUBSECTION_WINMESSAGE
        O_Question:MoveTo(nil)
        O_resetZone:MoveTo(nil)
        local text = getMessage(language, sec, subsec, 3) .. cr .. getMessage(language, sec, subsec, 4) .. TotalAnswers
        text = text .. getMessage(language, sec, subsec, 5) .. cr .. getMessage(language, sec, subsec, 6)
        text = text .. FalseAnswers .. getMessage(language, sec, subsec, 7) .. cr
        text = text .. getMessage(language, sec, subsec, 8) .. TooLate .. getMessage(language, sec, subsec, 9) .. cr
        text = text .. getMessage(language, sec, subsec, 10) .. cr .. getMessage(language, sec, subsec, 11)
        O_Finish.Description = text
        O_Finish:MoveTo(Player)
        T_Progress.Complete = true
        T_Progress.Active = true
        T_playingRules.Active = false
        T_playingRules.Display = false
        ZoneHaveToMove = false
        text = getMessage(language, sec, subsec, 1) .. Player.Name .. getMessage(language, sec, subsec, 2)
        msgMitNachrichtUndBild(text,
            P_Logo,
            function()
                Save()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end
