LANGUAGE_DE = 'de'
LANGUAGE_EN = 'en'
LANGUAGE_FR = 'fr'
LANGUAGE_ES = 'es'

SECTION_CARTRIDGE = "cartridge"
-- Generate for Each Item and Zone an own section
SECTION_ZONE_QUESTION = "zone-question"
SECTION_ITEM_FINSIH = "item-finish"
SECTION_ITEM_QUESTION = "item-question"
SECTION_ITEM_START_GAME = "item-start-game"
SECTION_ITEM_RESET_ZONE = "item-reset-zone"
SECTION_TASK_DOUBLE_TIME_JOKER = "task-double-time-joker"
SECTION_TASK_PROGRESS = "task-progress"
SECTION_TASK_CHANGE_QUESTION_JOKER = "task-change-question-joker"
SECTION_TASK_PLAYING_RULES = "task-playing-rules"
SECTION_INPUT_JOKER = "input-joker"
SECTION_TIMER_AKTUALISIERUNG = "timer-aktualisierung"
SECTION_TIMER_TIME_TO_ANSWER = "timer-time-to-answer"
SECTION_TIMER_QUESTION_JOKER = "timer-question-joker"
SECTION_TIMER_SECOND = "timer-second"
SECTION_TIMER_TEST_GPS = "timer-test-gps"
SECTION_FUNCTIONS = "functions"

SUBSECTION_QUESTIONHANDLING = "question-handling"
SUBSECTION_WINMESSAGE = "win-message"

SUBSECTION_CARTRIDGE_ON_START = "cartridge-on-start"
SUBSECTION_CARTRIDGE_ON_END = "cartridge-on-end"
SUBSECTION_CARTRIDGE_ON_RESUME = "cartridge-on-end"
SUBSECTION_CARTRIDGE_ON_SAVE = "cartridge-on-save"
SUBSECTION_OBJECT_NAME = "object-name"
SUBSECTION_OBJECT_DESCRIPTION = "object-description"
SUBSECTION_ZONE_ON_ENTER = "zone-on-enter"
SUBSECTION_ZONE_ON_EXIT = "zone-on-Exit"
SUBSECTION_ZONE_ON_DISTANT = "zone-on-distant"
SUBSECTION_ZONE_ON_PROXIMITY = "zone-on-proximity"
SUBSECTION_ZONE_ON_ACTIVITY_CHANGE = "zone-on-activity-change"
SUBSECTION_ITEM_ON_CLICK = "item-on-click"
SUBSECTION_TASK_ON_ACTIVE_CHANGED = "on-active-changed"
SUBSECTION_TASK_ON_COMPLETE_CHANGED = "task-on-complete-changed"
SUBSECTION_TASK_ON_CORRECTNESS_CHANGED = "task-on-correctness-changed"
SUBSECTION_TASK_ON_CLICK = "task-on-click"
SUBSECTION_INPUT_ON_GET_INPUT = "input-on-get-input"
SUBSECTION_INPUT_ON_ERROR = "input-on-error"
SUBSECTION_TIMER_ON_START = "timer-on-start"
SUBSECTION_TIMER_ON_STOP = "timer-on-stop"
SUBSECTION_TIMER_ON_ELAPSED = "timer-on-elapsed"
SUBSECTION_OTHER = "other"
-- Generate for each command an own section
SUBSECTION_FINISH_CMD01 = "finish-cmd01"
SUBSECTION_FINISH_CMD02 = "finish-cmd02"
SUBSECTION_FINISH_CMD03 = "finish-cmd03"
SUBSECTION_QUESTION_CMD01 = "question-cmd01"
SUBSECTION_QUESTION_CMD02 = "question-cmd02"
SUBSECTION_QUESTION_CMD03 = "question-cmd03"
SUBSECTION_QUESTION_CMD04 = "question-cmd04"
SUBSECTION_QUESTION_CMD05 = "question-cmd05"
SUBSECTION_QUESTION_CMD06 = "question-cmd06"
SUBSECTION_STARTGAME_CMD01 = "startgame-cmd01"
SUBSECTION_STARTGAME_CMD02 = "startgame-cmd02"
SUBSECTION_RESETZONE_CMD01 = "resetzone-cmd01"

-- General attributes
NAME = 'name'
DESCRIPTION = 'description'

messages = {
    ['de'] = {
        ['zone-question'] = {
            ['name'] = "Frage",
            ['description'] = "Es gibt noch keine Frage zu beantworten!",
            ['zone-on-enter'] = {
                [1] = ""
            }
        },
        ['item-start-game'] = {
            ['name'] = 'Spiel Starten',
            ['startgame-cmd02'] = {
                [1] = "Hallo ",
                [2] = " und herzlich Willkommen zu unserer heutigen Ausgabe von 'Wer wird Wissionaer'!",
                [3] = "Ihr sollt heute euer Wissen testen! Es ist nicht schlimm, wenn ihr mal eine Frage nicht beantworten koennt, ihr habt das Spiel desswegen nicht verloren! Ihr habt gewonnen, wenn ihr 20 Punkte erreicht habt. Wenn ihr eine Frage richtig beantwortet, dann werdet ihr mit +2 Punkten belohnt! Falls ihr die Frage falsch beantwortet, kriegt ihr -2 Punkte, wenn ihr 'nur' zu langsam seid, dann gibt es -1 Punkt!",
                [4] = "Um eine Frage zu beantworten, habt ihr 10 sek Zeit, davor bekommt ihr die Frage 20 sek gezeigt, in denen ihr entscheiden koennt, ob ihr sie beantworten wollt oder ob ihr einen Joker zusaetzlich als Hilfe benutzen moegt. Ihr habt insgesamt 3 Joker, verwendet sie weise!",
                [5] = "Ach so! Da wir ja keine Couchpotatoes sind, muss man zu jeder Frage 50 m laufen! Ausserdem solltet ihr darauf achten nicht zu speichern, waehrend ihr eine Frage offen habt, sonst wird diese aktive Frage als zu langsam gewertet. Diese Einschraenkung hat den Hintergrund, dass ihr die App nicht minimieren sollt, um zu googlen, waehrend ihr eine Frage beantwortet. Ansonsten koennt ihr jederzeit das Spiel verlassen und speichern.",
                [6] = "Und nun viel Spass!",
                [7] = "Noch eine Ergaenzung: Dies ist ein Quiz-WALK-Wherigo, das heisst, es ist egal wohin ihr lauft, die Zone richtet sich automatisch nach ein paar Sekunden aus. Sofern ihr euch genug bewegt habt, aendert sich die Richtung der naechsten Zone, falls nicht, koennt ihr die Zone mit dem Item 'Zone zuruecksetzen' wieder anstupsen, sodass sie sich bewegen sollte.",
                [8] = "So, ",
                [9] = " nun endgueltig viel Spass bei diesem Quiz-WALK-Wherigo!"
            }
        },
        ['item-finish'] = {
            ['name'] = "Abschluss",
            ['description'] = "",
            ['finish-cmd01'] = {
                ['name'] = "Final",
                [1] = "ACHTUNG!",
                [2] = "Es gibt bei diesem Wherigo zwei Logmoeglichkeiten:",
                [3] = "Die erste bei den Koordinaten: N 49* 28.829 E 010* 57.350 hat ein Passwortsatz, ein Zahlenschloss und Oeffnungszeiten.",
                [4] = "Die Oeffnungszeiten sind:",
                [5] = "Montag: geschlossen",
                [6] = "Dienstag: 10:00 Uhr bis 13:30 Uhr und 14:30 Uhr bis 18:00 Uhr",
                [7] = "Mittwoch: geschlossen",
                [8] = "Donnerstag: 10:00 Uhr bis 13:30 Uhr und 14:30 Uhr bis 18:00 Uhr",
                [9] = "Freitag: 10:00 Uhr bis 13:30 Uhr",
                [10] = "Samstag und Sonntag: geschlossen",
                [11] = "Die Angaben sind ohne Gewaehr und koennen im Internet bei der Stadt Fuerth nachgelesen werden. - https://www.fuerth.de/desktopdefault.aspx/tabid-948/1590_read-3884/",
                [12] = "Der Passwortsatz lautet: 'Geocacher sind lieber Wissionaer als Millionaer!'",
                [13] = "Um die Dose zu oeffnen, braucht ihr dann auch folgenden Zahlencode: 612!",
                [14] = "Die zweite Logmoeglichkeit fuer Cacher, die waehrend den Oeffnungszeiten verhindert sind, bieten wir folgende Logmoeglichkeit an:",
                [15] = "Nach dem Speichern hat euer Geraet (Ausnahme: I-Phone) eine .gwl oder .owl Datei angelegt. Schickt uns diese per E-Mail (4seul@gmx.de) zu. Als Betreff schreibt ihr bitte euren Cachernamen und die Koordinaten hinein. Ihr erhaltet dann von uns Logfreigabe und auf Wunsch uebernehmen wir den Logbucheintrag fuer euch.",
            },
            ['finish-cmd02'] = {
                ['name'] = 'Unlockcode',
                [1] = "Der Unlockcode lautet: ",
                [2] = "Falls der Unlockcode nicht funktionieren sollte, lasst einfach das letzte Zeichen weg."
            }
        },
        ['input-joker'] = {
            ['input-on-get-input'] = {
                [1] = "Die gestrichene Antwort ist: A",
                [2] = "Die gestrichene Antwort ist: B",
                [3] = "Die gestrichene Antwort ist: C"
            }
        },
        ['timer-test-gps'] = {
            ['timer-on-elapsed'] = {
                [1] = "GPS Test aktiv ",
                [2] = "GPS Test erfolgreich"
            }
        },
        ['functions'] = {
            ['question-handling'] = {
                [1] = "Schoen, wenn du schon wieder eine Frage beantworten magst, aber bitte warte, bis du in der naechsten Zone bist.",
                [2] = "Gut gemacht! Weiter zur naechsten Frage!",
                [3] = "Du hast ",
                [4] = " von 20 Punkten",
                [5] = "Schoen, dass du aufgewacht bist! Aber leider musst du dich noch gedulden, bis du in der naechsten Zone bist. Dann aber ja nicht zu lange brauchen ;) !",
                [6] = "Du warst zu langsam.",
                [7] = "Die richtige Antwort waere gewesen: ",
                [8] = "Schoen, dass du deinen Fehler ausbuegeln magst, aber du musst dich noch bis zur naechsten Zone gedulden!",
                [9] = "Schade, dass du diese Frage nicht richtig beantworten konntest!",
                [10] = "Du hast geantwortet: ",
            },
            ['win-message'] = {
                [1] = "Herzlichen Glueckwunsch! Du bist Wissionear! Zur Belohnung kannst du, ",
                [2] = ", nun den Final berechnen. Dazu findest du ein neues Item in deinem Menue!",
                [3] = "Hier jetzt mal eine kleine Statistik:",
                [4] = "Ihr habt insgesamt ",
                [5] = " Fragen beantwortet.",
                [6] = "Davon waren ",
                [7] = " falsch.",
                [8] = "Ihr wart bei ",
                [9] = " Fragen zu langsam.",
                [10] = "Danke euch fuer das Spielen! Ich hoffe, es hat euch Spass gemacht!",
                [11] = "E von SEUL"
            }
        }
    }
}


--[[
This method retrieves the message request via the function args.

language:	This argument should contain a two letter code of the language. If the language is unkown it will fallback
            to German.
section:	The section. Normally this is CARTRIDGE or a Cartridge Object
subsection:	The subsection. Normally this is an Event.
number:		The number starting at 1 until n.

return:		The string which was asked or if any of the last three parameters is wrong an empty string.
--]]
function getMessage(language, section, subsection, number)
    return messages [language] [section] [subsection] [number]
end
