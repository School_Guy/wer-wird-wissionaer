# How to contribute to this Wherigo Cache?

This Wherigo is currently German only and thus a bit hard to play for non German people. Thus translations are a good
start to help. Please have a look at the file `src/messages.lua`.

At some point it would be cool if this Wherigo gets some more questions as well as more caches placed through it. So
if you feel like it, I would be very glad if you could advertise it a bit in your area.

Apart from that this is a small project which is mostly done. Other topics which are possible in my eyes are:

- Logging with the official Wherigo Classes.
- Improving the precision of the timer (which will be hard due to Wherigo limitations)
