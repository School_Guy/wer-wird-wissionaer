# GC64N4G - Wer wird Wissionär

Corresponding Geocache: [GC64N4G](https://www.geocaching.com/geocache/GC64N4G_wer-wird-wissionar?guid=189ee2c6-8e29-413c-bfed-377e867f4bd7)

Corresponding Cartridge (wherigo.com): [Wer wird Wissionaer - Fuerth Edition](http://www.wherigo.com/cartridge/details.aspx?CGUID=055c4106-8f8e-42c6-af12-b31f7e36de63)

Corresponding Cartridge (Wherigo Foundation): none (currently)

This project is developed with [Urwigo](https://urwigo.cz).

The media files are in the same folder because for Urwigo is this the best case.